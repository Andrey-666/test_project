# Базовый образ, на основе которого будет создан локальный образ(ubuntu:20.04, python:3.7 и т.д.)
FROM python:3.10.2

# Копируем requirements.txt в контейнер в указанную директорию(Можно использовать ADD)
COPY requirements.txt /tmp
# С помощью RUN запускаем нужную нам команду
RUN pip install -r /tmp/requirements.txt

# Копируем содержимое проекта в указанную директорию
COPY . /opt/app
# Задаем директорию из которой будут выполняться все команды
WORKDIR /opt/app

# Запускаем наш сервер
CMD ["python", "manage.py", "runserver", "0:8000"]
